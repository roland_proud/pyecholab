.. currentmodule:: echolab._io.raw_reader

pyecholab basics
============


Typical Workflow
----------------

Working interactively with pyecholab generally uses the following steps

#.  Read data from disk
#.  Remove triangle-wave overlay (ES60 data only)
#.  Apply calibration settings
#.  Calculate derived quantities, such as Sv
#.  Transform data to desired reference frame.
#.  Manipulate data
#.  Save to disk

The RawReader Object
--------------------

The :class:`RawReader` object is a high-level interface for SIMRAD raw and bottom files. In addition to a 

  -  RawReader.config
        General configuration information pulled from file header

  -  RawReader.config['transceiers']
        Transciever configuration information keyed to transceiver index number

  -  RawReader.calibration_params
        User-defined calibration values

  -  RawReader.prefered_nmeas
        Prefered NMEA types for various functions (such as distance and gps interpolation)

  -  RawReader.data
        |:class:`pandas.DataFrame`| object containing sample data for each transceiver, indexed by channel number and timestamp

  -  RawReader.nmea
        :class:`pandas.DataFrame` object containing nmea strings, indexed by timestamp

  -  RawReader.bottom
        :class:`pandas.DataFrame` object containing botom depth information, indexed by timestamp.  Not intended to be used directly, see :member:`RawReader.interpolate_ping_bottom_range`

  -  RawReader.tags
        :class:`pandas.DataFrame` object containing annotation strings, indexed by timestamp

Reading Files
^^^^^^^^^^^^^

.. autoclass:: RawReader


Writing Files
^^^^^^^^^^^^^

.. automember:: RawReader.save


Pandas Objects
--------------

pandas provides a number of powerful data structures.  pyecholab leverages the :class:`pandas.DataFrame` object extensively for internal data management before the user solidifies a two-dimensional array with :member:`RawReader.to_array`.  


RawReader.data
^^^^^^^^^^^^^^

Ping header columns
-------------------
:param absorption_coefficient:
:param bandwidth:
:param channel:  channel ID string
:param count: number of samples in ping
:param file: original file name
:param file_ping: ping number in file
:param frequency: frequency in Hz
:param heading: ship heading
:param heave: ship heave
:param high_date: upper 64bit long of original timestamp
:param low_date: lower 64bit long of original timestamp
:param mode: Aquisition mode:  1: power, 2: angle, 3: power & angle
:param offset: Offset of first recorded sample
:param pitch: ship pitch
:param pulse_length: pulse length in seconds
:param roll: ship roll
:param sample_interval: sample interval in seconds
:param sound_velocity: sound velocity in m/s
:param spare0: unused
:param temperature: temperature in c
:param timestamp: original timestamp coverted from high_date & low_date
:param transducer_depth: transducer draft
:param transmit_power: transmit power in W
:param type: original datagram type
:param reduced_timestamp:  Reduced-resolution version of timestamp used for alignment

:param power: Raw indexed power as read from file
:param angle: Raw indexed angle as read from file

:param Sv:  log-Sv calculated using RawReader.Sv(linear=False, ...)
:param sv:  linear-sv calculated using RawReader.Sv(linear=True, ...)
:param Sp:  log-Sp calculated using RawReader.Sp(linear=False, ...)
:param sp:  linear-sp calculated using RawReader.Sp(linear=True, ...)
:param i_angle: Raw indexed angle split into alongship/athwartship
:param e_angle: Electrical angle split into alongship/athwartship
:param p_angle: Physical angle split into alongship/athwartship
