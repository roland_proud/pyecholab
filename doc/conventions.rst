
Important Conventions
=====================
It is important to keep the following conventions in mind when using pyecholab to work with acoustic data:

*  Range

	pyecholab uses range units exclusively when dealing with physical distance along the vertical sample axis (it is also possible to work in sample units).  Functions with the ability to handle depth units will convert these quantities internally to range for use.  Efforts have been made to remind the user of thise choice (for instance, the 'mask_*_range' functions in :class:`echolab.mask.EchogramMask`) 

	.. warning::  Again, for emphasis, **pyecholab uses range units**

*  Time

	pyecholab uses UTC time for all time-based quantities, because that's what any sane person should do.  SIMRAD echosounders attempt to do the same but are susceptible to improperly configured clocks on the original data acquisition computer.
