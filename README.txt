echolab is a python 2.7.x library for analysing acoustic data produced
by SIMRAD EK60, ES60 and ME70 echosounders.  It was developed under contract
by Zac Berkowitz for the National Oceanic and Atmospheric Administration (NOAA)

File IO:
    Reads/writes raw-data files (.raw, .bot, .out) for EK60 and ES60 echosounders.
    Support for ME70 files is experimental.

Calibration:
    Import/export EchoView .ecs calibration files.

Griding:
    Arbitrary referenced layers with 'surface', 'bottom', and 'transducer face' presets

Analysis:
    Single-beam/Split-beam mean Sv integrations

Masking:
    Sample-based masking by threshold
    Range-based masking (exclude above/below/between/outside range-referenced lines)
    Ping-based masking (exclude whole pings based on some function)

Misc:
    Remove triangle-wave offset present in SIMRAD ES60 data
    Check for and fix incorrect clock settings on original acquisition computer
    Interpolate/smooth GPS, vessel distance, and bottom depth values per-ping