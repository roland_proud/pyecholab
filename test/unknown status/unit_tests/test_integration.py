'''
Created on Mar 4, 2013

@author: zachary.berkowitz
'''

import echolab
import pandas as pd

from nose import tools as nt

RAW_FILE = '..\\data\\test_buddy_resize2-D20110712-T132550.raw'
BOT_FILE = '..\\data\\test_buddy_resize2-D20110712-T132550.bot'

data = echolab.RawReader([RAW_FILE, BOT_FILE], channel_names=['GPT  38 kHz 009072033fa2 1-1 ES38B'])
#Override transducer depth to 0
for x in range(1,6):
    data.calibration_params[x] = {'transducer_depth': 0}

data.interpolate_ping_bottom_range()
data.interpolate_ping_dist(nmea_type='VLW', ignore_checksum=True)
data.Sv(linear=True)

sv = data.to_array('sv', channel=1, drop_zero_range=True)
bottom_line = data.data.xs(1, level='channel').bottom.values

grid_params = echolab.grid.GridParameters(layer_spacing=50.0, layer_unit='range',
                    interval_spacing=1.0, interval_unit='distance')


ev_result_map = {'Sv_mean': 'Sv_mean',
                 'Sv_min': 'Sv_min',
                 'Sv_max': 'Sv_max',
                 'NASC': 'nasc',
                 'Height_mean': 'height_mean',
                 'Depth_mean': 'depth_mean',
                 'Thickness_mean': 'thickness_mean',
                 'Range_mean': 'range_mean',
                 'Samples': 'samples',
                 'ABC':'abc',
                 'Layer_depth_min':'depth_min',
                 'Layer_depth_max':'depth_max',
                 'Ping_S': 'ping_start',
                 'Ping_E': 'ping_end',
                 'Dist_S': 'start_distance',
                 'Dist_E': 'end_distance',
                 'Lat_E': 'end_lat',
                 'Lat_S': 'start_lat',
                 'Lon_E': 'end_lon',
                 'Lon_S': 'start_lon',
                 'start_timestamp': 'timestamp_start',
                 'end_timestamp': 'timestamp_end',
                 'Exclude_below_line_depth_mean':'mean_exclude_below', 
                 'Exclude_above_line_depth_mean':'mean_exclude_above',
                 
#                 'Minimum_Sv_threshold_applied':
#                 'Minimum_integration_threshold':
#                 'Maximum_Sv_threshold_applied':
#                 'Maximum_integration_threshold':,
#                 'Exclude_above_line_applied':
#                 'Exclude_above_line_depth_mean':
#                 'Exclude_below_line_applied':
#                 'Exclude_below_line_depth_mean':
#                 'Bad_data_samples':
#                 'No_data_samples':
#                 'Grid_reference_line':
#                 'Layer_top_to_reference_line_depth':
#                 'Layer_top_to_reference_line_range':
#                 'Layer_bottom_to_reference_line_depth':
#                 'Layer_bottom_to_reference_line_range':
#                 'Exclude_below_line_depth_min':
#                 'Exclude_below_line_range_min':
#                 'Exclude_below_line_depth_max':
#                 'Exclude_below_line_range_max'                 
}
class test_surface_sv_surface_ref_no_masking(object):
    
    
    
    @classmethod
    def setupClass(cls):
        global grid_params
        global sv
        
        cls.ev_csv = '..\\data\\test_buddy_resize2-D20110712-T132550-echoview-analysis-no-masking.csv'
        cls.ev_results = pd.read_csv(cls.ev_csv, index_col=[3, 4],
                parse_dates={'start_timestamp': [24, 25],
                             'end_timestamp': [26, 27]},
                skipinitialspace=True)
        cls.ev_results.index.names = [x.lower() for x in cls.ev_results.index.names]
        
        cls.grid = grid_params.grid(sv)
        cls.el_results, _ = echolab.integration.integrate_single_beam_Sv(sv, cls.grid)
        
#        cls.el_results.columns = [x.lower() for x in cls.el_results.columns]
        

    def test_fields(self):
        
        fields = [('Sv_mean', 0.001),
                  ('Sv_max', 0.01),
                  ('Sv_min', 0.001),
                  ('Ping_E', 1),
                  ('Ping_S', 1),
                  ('ABC',  0.1),
                  ('Thickness_mean', 1e-4),
                  ('Range_mean', 1e-4),
                  ('Depth_mean', 1e-3),
                  ('Height_mean', 1e-4),
                  ('Exclude_below_line_depth_mean', 1),
                  ('Exclude_above_line_depth_mean', 1),
                  ]
        
        for field, tolerence in fields:
            yield self.check_field, field, tolerence
 
    def check_field(self, field, tolerence):
        
        el_field = ev_result_map[field]
        deviations = abs(self.ev_results[field] - self.el_results[el_field])
        bad_indxs = deviations > tolerence

        if bad_indxs.any():
            raise ValueError('%d interval-layer pairs failed to pass %s, w/ max deviation of %f' %(len(bad_indxs[bad_indxs]), el_field,
                                deviations.max()))
            

class test_surface_sv_bottom_ref_no_masking(object):
    
    
    
    @classmethod
    def setupClass(cls):
        global grid_params
        global bottom_line
        global sv
        
        cls.ev_csv = '..\\data\\test_buddy_resize2-D20110712-T132550-echoview-analysis-no-masking-bottom-referenced.csv'
        cls.ev_results = pd.read_csv(cls.ev_csv, index_col=[3, 4],
                parse_dates={'start_timestamp': [24, 25],
                             'end_timestamp': [26, 27]},
                skipinitialspace=True)
        
        cls.ev_results.index.names = [x.lower() for x in cls.ev_results.index.names]
        
        cls.grid = grid_params.grid(sv, reference=bottom_line)
        cls.el_results, _ = echolab.integration.integrate_single_beam_Sv(sv, cls.grid)
        
#        cls.el_results.columns = [x.lower() for x in cls.el_results.columns]
        

    def test_fields(self):
        
        fields = [('Sv_mean', 5),
                  ('Sv_max', 5),
                  ('Sv_min', 5),
                  ('Ping_E', 1),
                  ('Ping_S', 1),
                  ('ABC',  5),
                  ('Thickness_mean', 1e-1),
                  ('Range_mean', 1e-1),
                  ('Depth_mean', 1e-1),
                  ('Height_mean', 1e-1),
                  ('Exclude_below_line_depth_mean', 1),
                  ('Exclude_above_line_depth_mean', 1),
                  ]
        
        for field, tolerence in fields:
            yield self.check_field, field, tolerence
 
    def check_field(self, field, tolerence):
        
        el_field = ev_result_map[field]
        deviations = abs(self.ev_results[field] - self.el_results[el_field])
        bad_indxs = deviations > tolerence
        if bad_indxs.any():
            raise ValueError('%d interval-layer pairs failed to pass %s, w/ max deviation of %f' %(len(bad_indxs[bad_indxs]), el_field,
                                deviations.max()))

class test_surface_sv_surface_ref_with_masking(object):
    
    
    
    @classmethod
    def setupClass(cls):
        global grid_params
        global sv
        
        cls.ev_csv = '..\\data\\test_buddy_resize2-D20110712-T132550-echoview-analysis-with-masking.csv'
        cls.ev_results = pd.read_csv(cls.ev_csv, index_col=[3, 4],
                parse_dates={'start_timestamp': [24, 25],
                             'end_timestamp': [26, 27]},
                skipinitialspace=True)
        cls.ev_results.index.names = [x.lower() for x in cls.ev_results.index.names]
        
        cls.grid = grid_params.grid(sv)
        
        cls.mask = echolab.mask.EchogramMask()
        cls.mask.mask_above_range(name='exclude_above', reference=5.0)
        cls.mask.mask_below_range(name='exclude_below', reference=bottom_line-3.0)
        cls.el_results, cls.mask_results = echolab.integration.integrate_single_beam_Sv(sv, cls.grid, mask=cls.mask)
#        cls.el_results.columns = [x.lower() for x in cls.el_results.columns]
        

    def test_fields(self):
        
        fields = [('Sv_mean', 0.02),
                  ('Sv_max', 0.01),
                  ('Sv_min', 0.001),
                  ('Ping_E', 1),
                  ('Ping_S', 1),
                  ('ABC',  0.1),
                  ('Thickness_mean', 1e-3),
                  ('Range_mean', 1e-2),
                  ('Depth_mean', 1e-2),
                  ('Height_mean', 1e-3),
                  ('Exclude_below_line_depth_mean', 1),
                  ('Exclude_above_line_depth_mean', 1),
                  ]
        
        for field, tolerence in fields:
            yield self.check_field, field, tolerence
 
    def check_field(self, field, tolerence):
        
        el_field = ev_result_map[field]
        deviations = abs(self.ev_results[field] - self.el_results[el_field])
        bad_indxs = deviations > tolerence

        if bad_indxs.any():
            raise ValueError('%d interval-layer pairs failed to pass %s, w/ max deviation of %f' %(len(bad_indxs[bad_indxs]), el_field,
                                deviations.max()))
            
class test_surface_sv_bottom_ref_with_masking(object):
        
    @classmethod
    def setupClass(cls):
        global grid_params
        global sv
        
        cls.ev_csv = '..\\data\\test_buddy_resize2-D20110712-T132550-echoview-analysis-with-masking-bottom-referenced.csv'
        cls.ev_results = pd.read_csv(cls.ev_csv, index_col=[3, 4],
                parse_dates={'start_timestamp': [24, 25],
                             'end_timestamp': [26, 27]},
                skipinitialspace=True)
        cls.ev_results.index.names = [x.lower() for x in cls.ev_results.index.names]
        
        cls.grid = grid_params.grid(sv, reference=bottom_line)
        
        cls.mask = echolab.mask.EchogramMask()
        cls.mask.mask_above_range(name='exclude_above', reference=5.0)
        cls.mask.mask_below_range(name='exclude_below', reference=bottom_line - 3)
        
        cls.el_results, cls.mask_results = echolab.integration.integrate_single_beam_Sv(sv, cls.grid, mask=cls.mask)
#        cls.el_results.columns = [x.lower() for x in cls.el_results.columns]
        

    def test_fields(self):
        
        fields = [('Sv_mean', 1),
                  ('Sv_max', 1),
                  ('Sv_min', 1),
                  ('Ping_E', 1),
                  ('Ping_S', 1),
                  ('ABC',  0.1),
                  ('Thickness_mean', 1e-1),
                  ('Range_mean', 1e-1),
                  ('Depth_mean', 1e-1),
                  ('Height_mean', 1e-1),
                  ('Exclude_below_line_depth_mean', 1),
                  ('Exclude_above_line_depth_mean', 1),
                  ]
        
        for field, tolerence in fields:
            yield self.check_field, field, tolerence
 
    def check_field(self, field, tolerence):
        
        el_field = ev_result_map[field]
        deviations = abs(self.ev_results[field] - self.el_results[el_field])
        bad_indxs = deviations > tolerence

        if bad_indxs.any():
            raise ValueError('%d interval-layer pairs failed to pass %s, w/ max deviation of %f' %(len(bad_indxs[bad_indxs]), el_field,
                                deviations.max()))