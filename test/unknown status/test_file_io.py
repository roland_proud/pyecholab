import numpy as np
import os
from echolab.simrad_io import ER60Data
from nose import tools as nt

RAW_FILENAME = 'data\\test_buddy_resize2-D20110712-T132550.raw'


er60_data = ER60Data(RAW_FILENAME, frequencies=[38e3])

def test_update():

	Sp = er60_data.pings[0].Sp(gps_checksums=False)
	Sp += 30.0

	tmpfile = '_update_test.raw'
	er60_data.update(Sp)

	er60_data.to_file(tmpfile)

	new_data = ER60Data(tmpfile, frequencies=[38e3])

	new_Sp = new_data.pings[0].Sp(gps_checksums=False)

	diff = np.abs(new_Sp._data[~np.isnan(new_Sp)] - Sp._data[~np.isnan(Sp)])

	nt.assert_true((diff - 30 <= 1).all())
	
	os.remove(tmpfile)




