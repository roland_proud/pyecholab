'''
Created on Mar 19, 2012

@author: zachary.berkowitz
'''
from echolab.util import grid_data
from echolab.mask import EchogramCompositeMask
from echolab.simrad_io import ER60Data
from echolab.analysis import integration
from csv import reader as csv_reader

from nose import tools as nt
RAW_FILENAME = 'data\\test_buddy_resize2-D20110712-T132550.raw'



INTERVAL = 1
LAYER = 2
MEAN_SV = 3
MEAN_DEPTH = 6
MEAN_HEIGHT = 5
PING_START = 9
PING_END = 10
 
echoview_csv_dict={'interval': 1,
               'layer': 2,
               'nasc': 4,
               'mean_sv': 3,
               'mean_depth': 6,
               'mean_height': 5,
               'ping_start': 9,
               'ping_end': 10}    

er60_data = ER60Data(RAW_FILENAME, frequencies=[38e3], smooth_bottom_data=True)

def test_grid_integration_no_mask():

    ECHOVIEW_RESULTS = 'data\\test_buddy_resize2-D20110712-T132550-echoview-analysis-no-masking.csv'
    DISTANCE_SPAN = 1.0
    DEPTH_SPAN = 100.0
    
    
    echoview_results = []
    global er60_data
    
    with open(ECHOVIEW_RESULTS, 'r') as fid:
        csv = csv_reader(fid)
        header = csv.next()    
        for cell in csv:
            echoview_results.append(cell)
    
    

  
        
    linear_sv = er60_data.pings[0].Sv(linear=True, gps_checksums=False)
    
    integration_grid = grid_data(linear_sv, interval_spacing=DISTANCE_SPAN,
                                  layer_spacing=DEPTH_SPAN, 
                                  return_partial=True,
                                  shift_first_row=False)
    


    integration_results = integration.integrate_sv(linear_sv,
                                       grid=integration_grid,
                                       mask=None,
                                       adaptive_thickness_mean=False,
                                       return_log=True)
    
    for eview_result in echoview_results:
        
        col = int(eview_result[INTERVAL]) - 1
        row = int(eview_result[LAYER]) - 1
       
        py_result = integration_results[row, col]
        

        #print row + 1, col + 1, ev_mean_sv, my_mean_sv, ev_mean_sv-my_mean_sv
        nt.assert_almost_equals(py_result['mean_sv'], 
                                float(eview_result[MEAN_SV]), delta=0.1)

        nt.assert_almost_equals(py_result['mean_height'],
                                float(eview_result[MEAN_HEIGHT]), delta=1)

        nt.assert_almost_equals(py_result['mean_depth'],
                                float(eview_result[MEAN_DEPTH]), delta=1)
        
        
def test_grid_integration_with_mask():

    ECHOVIEW_RESULTS = 'data\\test_buddy_resize2-D20110712-T132550-echoview-analysis-with-masking.csv'
    DISTANCE_SPAN = 1.0
    DEPTH_SPAN = 100.0
    BOTTOM_OFFSET = -3
    RINGDOWN_RANGE = 5
    
    echoview_results = []
    global er60_data
    
    with open(ECHOVIEW_RESULTS, 'r') as fid:
        csv = csv_reader(fid)
        header = csv.next()    
        for cell in csv:
            echoview_results.append(cell)
    
    

  
        
    linear_sv = er60_data.pings[0].Sv(linear=True, gps_checksums=False)
    
    integration_grid = grid_data(linear_sv, interval_spacing=DISTANCE_SPAN,
                                  layer_spacing=DEPTH_SPAN, 
                                  return_partial=True,
                                  shift_first_row=True)
 
    # bottom_ranges = er60_data.pings[0].get_bottom_data()
    bottom_ranges = linear_sv.bottom_ranges
    exclusion_mask = EchogramCompositeMask(linear_sv)
    exclusion_mask.mask_above_constant_range('ringdown', range=RINGDOWN_RANGE,
                                             by_sample_number=False)
    
    exclusion_mask.mask_below_varying_range('bottom_mask_offset', 
                                            range=bottom_ranges,
                                            offset=BOTTOM_OFFSET)    

    integration_results = integration.integrate_sv(linear_sv,
                                       grid=integration_grid,
                                       mask=exclusion_mask,
                                       adaptive_thickness_mean=False,
                                       return_log=True)
    
    for eview_result in echoview_results:
        
        col = int(eview_result[INTERVAL]) - 1
        row = int(eview_result[LAYER]) - 1
       
        py_result = integration_results[row, col]
        #print row + 1, col + 1, ev_mean_sv, my_mean_sv, ev_mean_sv-my_mean_sv
        nt.assert_almost_equals(py_result['mean_sv'], 
                                float(eview_result[MEAN_SV]), delta=0.5)

        nt.assert_almost_equals(py_result['mean_height'],
                                float(eview_result[MEAN_HEIGHT]), delta=1)

        nt.assert_almost_equals(py_result['mean_depth'],
                                float(eview_result[MEAN_DEPTH]), delta=1)
#    for k in integration_results:
#        print k['mean_sv'], k['nasc'], k['mean_height'], k['mean_depth'], k['n_samples']
        
    
#from nose.tools import assert_almost_equal
#import echolab
#from echolab.analysis import integration
#from os import path as opath
#
#RAW_FILENAME = opath.join('.', 'data', 'test_buddy_resize2-D20110712-T132550.raw')
#
#def test_integration_nomask():
#    
#    er60_data = echolab.ER60Data()
#    er60_data.from_file(RAW_FILENAME, desired_frequencies=[38e3], load_bot_data=False) 
#
#    cells = integration.integrate_EK60_by_distance(er60_data, channel_id=0, 
#        exclusion_mask=None, return_log=True, distance_interval=1, 
#        depth_interval=100.0, skip_first_row=True)
#    
#    
#    csv_filename = opath.join('.', 'data', 'echoview_analysis_by_region.csv')
#    with open(csv_filename, 'rb') as csv_fid:
#        rows = csv_fid.readlines()
#        
#
#    header = map(lambda x: x.strip(), rows[0].strip('\r\n').split(','))
#    
#    sv_mean_indx = header.index('Sv_mean')
#    nasc_indx = header.index('NASC')
#    interval_indx = header.index('Interval')
#    layer_indx = header.index('Layer')
#    
#    for row in rows[1:]:
#        fields = map(lambda x: x.strip(), row.strip('\r\n').split(','))
#        interval = int (fields[interval_indx])
#        layer = int (fields[layer_indx])
#        sv_mean = float(fields[sv_mean_indx])
#        nasc = float(fields[nasc_indx])
#         
#        cell_indx = 5*(interval - 1) + layer - 1        
#        cell = cells[cell_indx]
#        
#        assert_almost_equal(sv_mean, cell['mean_sv'], delta=0.1)
#        assert_almost_equal(nasc, cell['NASC'], digits=)
#
#        print 'NASC:  ',  nasc, cell['NASC']
#        
#        
#if __name__ == '__main__':
#    test_integration_nomask()