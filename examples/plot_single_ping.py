# -*- coding: utf-8 -*-
"""

@author: rick.towler

plot_single_ping is a simple example that demonstrates accessing various data
in the pyEcholab AxesArray. It also demonstrates using some basic numpy functions
and plotting with matplotlib. This example plots a single ping, smoothes the ping
with a hanning window, plots the smoothed ping, finds the max Sv of the smoothed
ping, then finds the envelope of the "max" echo as defined by the maximum and
an Sv backstep.

"""

import echolab
import numpy
from matplotlib import pyplot as plt


#  specify the ping number to plot
ping_number = 276

#  specify the minimum range that we'll consider searching for the max Sv. Set
#  this to a value that will put the search beyond the range of the ringdown.
min_range = 5

#  specify the width of the Hanning window (smoothing window)
window_len = 11

#  specify the backstep in dB. This value is subtracted from the max Sv and the
#  result is used to specify the lower bounds of the echo envelope we plot
Sv_backstep = 50

#  define the path to the data file
raw_filename = 'C:/Program Files (x86)/Simrad/Scientific/ER60/Examples/Survey/OfotenDemo-D20001214-T145902.raw'




def get_echo_envelope(data, echo_peak, threshold, sample_vector, range_vector,
                      contiguous=True, range_min=2):
    '''
    get_echo_envelope calculates the near and far edges of an echo defined by
    the provided peak sample value and a threshold value. You must also provide
    the ping vector, sample vector, and range vector.

    '''

    #  find the far side of the echo envelope
    far_envelope_samples = sample_vector[data[echo_peak:] > threshold] + echo_peak

    #  find the farthest sample that exceeds the threshold
    if contiguous:
        #  find the farthest contiguous sample
        #  first check if any of the differences between elements is greater than 1
        sample_diff = numpy.where(numpy.diff(far_envelope_samples) > 1)
        if (sample_diff[0].size > 0):
            #  there is a break - find the farthest sample up to the break
            max_idx = numpy.min(sample_diff)
            #  convert it to sample number
            max_sample = far_envelope_samples[max_idx]
        else:
            #  there is no break, get the farthest sample greater than the threshold
            max_sample = far_envelope_samples[-1]
    else:
        #  just return the farthest sample above threshold - doesn't have to be
        #  contiguous.
        max_sample = far_envelope_samples[-1]

    #  get the next sample too
    next_sample = max_sample + 1

    #  calculate the interpolated range for our far envelope edge
    max_range = numpy.interp(threshold, [data[next_sample], data[max_sample]],
                             [range_vector[next_sample], range_vector[max_sample]])
    #  set the max Sv to our threshold
    max_Sv = threshold

    #  extract the sample numbers that make up the far side of the envelope
    far_envelope_samples = far_envelope_samples[far_envelope_samples <= max_sample]


    #  calculate the lower search bound - usually you will at least want to
    #  avoid the ringdown.
    lower_bound = sample_vector[range_vector > range_min].min()
    #  then find the lower bound of the envelope
    near_envelope_samples = (echo_peak - sample_vector[data[echo_peak:lower_bound:-1] > threshold])

    if contiguous:

        sample_diff = numpy.where(numpy.diff(near_envelope_samples) < -1)
        if (sample_diff[0].size > 0):
            min_idx = numpy.min(sample_diff)
            min_sample = near_envelope_samples[min_idx]
        else:
            min_sample = near_envelope_samples[-1]
    else:
        min_sample = near_envelope_samples[-1]

    #  and the next one
    previous_sample = min_sample - 1

    #  calculate the interpolated range for our near envelope edge
    min_range = numpy.interp(threshold, [data[previous_sample], data[min_sample]],
                             [range_vector[previous_sample], range_vector[min_sample]])
    #  set the max Sv to our threshold
    min_Sv = threshold

    #  extract the sample numbers that make up the near side of the envelope
    near_envelope_samples = near_envelope_samples[near_envelope_samples >= min_sample]
    #  flip them so our composite envelope samples array is in order and drop the
    #  peak sample so it isn't duplicated.
    near_envelope_samples = near_envelope_samples[:0:-1]

    #  create a single array containing the combined envelope samples
    envelope_samples = numpy.concatenate([near_envelope_samples,
                                          far_envelope_samples])

    return (envelope_samples, (min_range, min_Sv, max_range, max_Sv))


if __name__ == "__main__":

    #  read in the .raw data file
    print('Reading raw file %s' % (raw_filename))
    raw_data = echolab._io.raw_reader.RawReader(raw_filename)

    #  Calculate Sv
    print('Calculating Sv')
    raw_data.Sv(linear=False)

    #  get an AxesArray containing Sv from the first channel
    Sv = raw_data.to_array('Sv', channel=1, drop_zero_range=False)

    #  extract a single ping
    Sv_ping = Sv[:,ping_number]

    #  determine the maximum Sv beyond the specified minimum range
    max_Sv = Sv_ping[Sv.axes[0]['range'] > min_range].max()

    #  determine the range at our maximum
    max_Sv_range = Sv.axes[0]['range'][Sv_ping == max_Sv]

    #  create a figure
    fig = plt.figure(figsize=(5,8))

    #  plot the Sv
    plt.plot(Sv_ping, Sv.axes[0]['range'], color=(0,1,0,0.2))

    #  now plot the maximum Sv as a red circle
    plt.plot(max_Sv, max_Sv_range, 'o',  color=(0,1,0,0.2))

    #  smooth ping
    hanning_window = numpy.hanning(window_len)
    smoothed_ping = numpy.convolve(hanning_window/hanning_window.sum(), Sv_ping, mode='same')

    #  plot the smoothed ping
    plt.plot(smoothed_ping,  Sv.axes[0]['range'], 'b')

    #  determine the maximum Sv of the smoothed ping
    max_Sv = smoothed_ping[Sv.axes[0]['range'] > min_range].max()

    #  determine the range at our maximum on the smoothed ping
    max_Sv_range = Sv.axes[0]['range'][smoothed_ping == max_Sv]

    #  get the sample number at the max
    sample_max = Sv.axes[0]['sample'][smoothed_ping == max_Sv]

    #  calculate the threshold that will define the lower bound (in Sv) of our
    #  echo envelope.
    threshold = max_Sv - Sv_backstep

    #  get the echo envelope
    (envelope_samples, envelope_edges) = get_echo_envelope(smoothed_ping,
                            sample_max, threshold, Sv.axes[0]['sample'], Sv.axes[0]['range'],
                            contiguous=True)

    #  place markers on the envelope sample locations
    env_Sv = smoothed_ping[Sv.axes[0]['sample'][envelope_samples]]
    env_range = Sv.axes[0]['range'][envelope_samples]
    plt.plot(env_Sv, env_range, 'o', color=[1,0,0,0.5])

    #  place markers on the min and max envelope locations as well as the envelope peak
    plt.plot(envelope_edges[3], envelope_edges[2], 'bo')
    plt.plot(envelope_edges[1], envelope_edges[0], 'bo')
    plt.plot(max_Sv, max_Sv_range, 'bo')

    #  plot the threshold
    plt.plot([threshold,threshold], [Sv.axes[0]['range'].min(),
              Sv.axes[0]['range'].max()], color=(0,0,0,0.2))

    #  set the limits on the plot axes
    plt.gca().set_ylim(Sv.axes[0]['range'].max(),
                       Sv.axes[0]['range'].min())
    plt.gca().set_xlim(Sv_ping.min(),
                       Sv_ping.max())

    #  label the figure
    plt.ylabel('range (m)')
    plt.xlabel('Sv')
    title = '%s \n Ping %i' % (Sv.info['transceiver_info']['channel_id'], ping_number)
    fig.suptitle(title, fontsize=18)

    #  and show
    plt.show(block=True)




