# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 20:42:23 2014

@author: rtowler

An example showing how to remove the systematic triangle wave error injected into
Simrad ES60 data files. This example illustrates using the echolab.AxesArray
object as well as the raw_reader.update_from_array and raw_reader.save methods.

"""

import echolab
import numpy as np
from matplotlib import pyplot as plt
import logging


#  set up logging - used to give feedback from the ecolab package
logging.basicConfig(level=logging.DEBUG)



#  define the paths to an ESxx data file
raw_filename = 'D:/test_data/L0003-D20140607-T152725-ES60.raw'
raw_filename = 'D:/test_data/L0003-D20140607-T144320-ES60.raw'
#raw_filename = 'D:/test_data/L0003-D20140606-T124221-ES60.raw'
#bot_filename = 'D:/test_data/L0003-D20140606-T124225-ES60.out'


#  read in the .raw data file and the bottom file - this returns a "RawReader"
#  object that contains the data.
raw_data = echolab._io.raw_reader.RawReader([raw_filename])

#  calculate inter-ping distances using GPS data or VLW NMEA datagrams
raw_data.interpolate_ping_dist(ignore_checksum=True)

#  get the power array for channel 1 - This makes a copy of the requested data
#  and returns it as an echolab.AxesArray object. Most of the time you will work
#  data in this form as it is easier to work with than directly accessing data
#  in the raw_data object.
power = raw_data.to_array('power', channel=1)

'''
the AxesArray object holds a 2d masked array of containing the data you requested
for the channel that you specified. It also has 2 additional properties containing
important meta-data.

The .info dictionary contains the following:
    data_type:            type of data, 'sv', 'Sv', etc.
    meters_per_sample:    sample thickness
    sample_interval:      sample interval in seconds
    sound_velocity:       sound velocity in m/s
    transceiver_info:     copy of the transceiver config

The .axes list will be populted by two record arrays:

    .axes[0] will contain a record array w/ fields relating to samples
        range:         range from reference in meters
        sample:        sample number from reference

    .axes[1] will also be a record array w/ fields relating to pings:
        reduced_timestamp:     timestamp of each ping
        reference:             range reference used to create array
        ping:                  ping number
        offset:                sample offset from original data offset
                               in ping
        count:                 number of samples after 'offset' contain
                               data
        shift:                 location of first sample in
                               .axes[0]['sample'] units
        transducer_depth:      depth of transducer face in meters
        surface:               range of surface from reference.  Can
                               be negative if reference = 'surface'
                               (which is really the first sample)
                               and the transducer_depth =/= 0

    .axes[1] may also have the optional fields if the corresponding
    quantitites have been interpolated previously:
        lat:                   ping latitude
        lon:                   ping longitude
        distance:              vessel distance in nmi
        bottom:                range of bottom return

Here are a few examples of accessing data in the array:

  List the sample numbers of samples 100-150 in the data array:
      power.axes[0]['sample'][99:150]

  List the range of samples 100-150 in the data array:
      power.axes[0]['range'][99:150]

  List the ping times for the first 10 pings:
      power.axes[1]['reduced_timestamp'][0:10]

  Show the distance covered over the extent of the data
      power.axes[1]['distance'][-1]

'''

#  Extract the 2nd and 3rd samples, average them, and then extract just the
#  data (we don't care about the mask...) Remember Python uses 0 based indexing
#  and range ends are non-inclusive which is why samples 2 and 3 are indexed as
#  "1:3". ":" in the 2nd dimension extracts all of the pings.
uncorrected_wave = power[1:3,:].mean(axis=0).data

#  for clarity, extract the ping times
ping_times = power.axes[1]['reduced_timestamp'][:]

#  plot the uncorrected data
plt.plot(ping_times, uncorrected_wave)

#  remove triangle wave - You must provide ->power<- data to correct_triwave.
#  it will raise an error if you provide a derived data type.
fit_results = echolab.util.triwave.correct_triwave(power)

#  plot the tri wave we fit to our data. I'm reaching a bit deep into echolab
#  to generate the fit_wave. Normally you wouldn't need to do this but you can
#  check the util.triwave source for details on this function.
n = np.arange(0,len(uncorrected_wave))
fit_wave = echolab.util.triwave.general_triangle(n, A=fit_results['amplitude'],
        k=round(fit_results['period_offset']), C=fit_results['amplitude_offset'])
plt.plot(ping_times, fit_wave, 'r')

#  now plot the corrected data in green
corrected_wave = power[1:3,:].mean(axis=0).data
plt.plot(ping_times, corrected_wave, 'g')

#  show the plot and wait here until window is closed
plt.show(block=True)

#  push data back into RawReader object - It's important to point out the
#  "update_raw" keyword. Setting this to True will update the underlying raw data
#  if the data you are pushing back is a derived quantity. For example, if I
#  extract Sv data, manipulate it in some way, then push it back with update_raw
#  set to True, the Sv data will be converted to power and the power values will
#  be replaced with these new data. If I push it back with update_raw set to False
#  the original power values will remain unchanged.
#
#  Note that if you are pushing back "raw" data (power or i_angles) as in this example
#  then update_raw is effectively true regardless since you are updating the raw
#  data directly. I'm setting update_raw=True here simply to show this keyword exists.
raw_data.update_from_array(power, update_raw=True)

#  write data back to disk. We provide a suffix which is appended to the output
#  file name and we specify that we should overwrite the file if it exists.
raw_data.save(suffix='_tri_corrected', overwrite=True)

print('done')

