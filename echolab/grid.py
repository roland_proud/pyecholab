﻿# coding=utf-8

# Copyright (c) 2012, Zac Berkowitz
#     National Oceanic and Atmospheric Administration (NOAA)
#     Alaskan Fisheries Science Center (AFSC)
#     Resource Assessment and Conservation Engineering (RACE)
#     Midwater Assessment and Conservation Engineering (MACE)

# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.

# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation 
#     and/or other materials provided with the distribution.

# 3.  Neither the names of NOAA, AFSC, RACE, or MACE nor the names of its 
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.


# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

u'''
.. :module:: echolab.grid

    :synopsis: Used to grid data for integration analyses


example::

    import echolab
    from echolab import plotting

    raw_file = 'path/to/some/file.raw'
    bot_file = 'path/to/some/file.out'

    data = echolab.RawReader([raw_file, bot_file])
    data.interpolate_ping_bottom_depth()

    #Compute Sv values
    data.Sv()

    #Get surface-referenced Sv values for channel 1
    Sv = data.to_array('Sv', channel=1)

    #Setup grid for 10m layers, 1nmi intervals
    grid_params = echolab.GridParameters(layer_spacing=10, interval_spacing=1.0,
        layer_unit='range', interval_unit='distance')

    #Create grid
    grid = grid_params.grid(Sv)

    #Create plot
    plotting.simple_echogram_plot(Sv, grid=grid)


| Developed by:  Zac Berkowitz <zac.berkowitz@gmail.com> under contract for
| National Oceanic and Atmospheric Administration (NOAA)
| Alaska Fisheries Science Center (AFSC)
| Midwater Assesment and Conservation Engineering Group (MACE)
|
| Maintained by:
|       Zac Berkowitz <zac.berkowitz@gmail.com>
|       Rick Towler   <rick.towler@noaa.gov>
'''

import pandas as pd
import logging
import numpy as np

log = logging.getLogger(__name__)

__all__ = ['GridParameters']


class GridParameters(object):
    '''
    Grid parameter object used to grid data for integration analyses.

    To grid data using this instantiated object, call
    :meth:`GridParameters.grid`
    '''
    def __init__(self, layer_spacing=None, interval_spacing=None,
                 layer_unit='range', interval_unit='distance'):
        '''
        :param interval_spacing: Spacing between intervals
        :type interval_spacing: float

        :param layer_spacing: Spacing between layers
        :type layer_spacing: float

        :param interval_unit:  Interval units
        :type interval_unit: str

        :param layer_unit: Layer units
        :type layer_unit: str

        Valid unit types (choose of of the following):
        layer_unit:  ['range', 'sample']
        interval_unit:  ['ping', 'distance']
        '''

        valid_layer_units = ['range', 'sample']
        valid_interval_units = ['ping', 'distance']

        if layer_unit.lower() not in valid_layer_units:
            err_str = '%s is not a valid layer unit, choose one of %s' % \
                             (layer_unit, str(valid_layer_units))
            raise ValueError(err_str)

        if interval_unit.lower() not in valid_interval_units:
            err_str = '%s is not a valid layer unit, choose one of %s' % \
                             (interval_unit, str(valid_interval_units))
            raise ValueError(err_str)

        self.layer_spacing = layer_spacing
        self.layer_unit = layer_unit.lower()
        self.interval_spacing = interval_spacing
        self.interval_unit = interval_unit.lower()

    def grid(self, data, reference=None):

        try:
            ranges = data.axes[0][self.layer_unit]
            layer_unit = self.layer_unit
        except AttributeError:
            log.warning('Using data shape for layer vector')
            ranges = pd.np.arange(data.shape[0])
            layer_unit = 'sample'

        try:
            pings = data.axes[1][self.interval_unit]
            interval_unit = self.interval_unit
        except AttributeError:
            log.warning('Using data shape for interval vector')
            pings = pd.np.arange(data.shape[1])
            interval_unit = 'ping'

        if reference is None:
            try:
                reference = data.axes[1]['reference']
            except AttributeError:
                log.warning('No reference provided or found in data, using first sample')
                reference = pd.np.zeros((data.shape[1],)) + ranges[0]

        else:
            if isinstance(reference, (int, float)):
                reference = np.zeros(data.shape[1], 'float') + reference
            
            
            elif len(reference) == data.shape[1]:
                pass

            elif len(reference) > data.shape[1]:
                log.warning('Provided reference is longer than data, truncating to fit')
                reference = reference[:data.shape[1]]

            else:
                raise IndexError('Provided reference is shorter than data.')

        if self.interval_spacing is None:
            ping_intervals = pd.np.ones((data.shape[1]))

        else:
            ping_intervals = \
                pd.np.floor((pings - pings[0]) / self.interval_spacing).astype('int') + 1
                
        
        intervals = pd.np.unique(ping_intervals)
        interval_upper_bounds = pd.np.empty((len(intervals),), dtype='int')

        for k in range(len(intervals)):
            interval_upper_bounds[k] = \
                pd.np.argwhere(ping_intervals == intervals[k])[-1]

        interval_lower_bounds = pd.np.zeros_like(interval_upper_bounds)
        interval_lower_bounds[1:] = interval_upper_bounds[:-1] + 1
        
        try:
            ping_info  = data.axes[1]
           
            first_ping = ping_info[interval_lower_bounds]['ping']
            last_ping = ping_info[interval_upper_bounds]['ping']
     
            interval_info = dict(interval=intervals,
                                 start_timestamp=ping_info['reduced_timestamp'][interval_lower_bounds],
                                 end_timestamp=ping_info['reduced_timestamp'][interval_upper_bounds],
                                 start_ping=first_ping,
                                 end_ping=last_ping)
            
            if 'distance' in ping_info.dtype.names:
                interval_info['start_distance'] = ping_info[interval_lower_bounds]['distance']
                interval_info['end_distance'] = ping_info[interval_upper_bounds]['distance']
#                interval_info['end_distance'] = pd.np.zeros_like(interval_info['start_distance'])
#                interval_info['end_distance'][:-1] = ping_info[interval_upper_bounds[:-1] + 1]['distance']
#                interval_info['end_distance'][-1] = ping_info[interval_upper_bounds[-1]]['distance']
#                
            if 'lat' in ping_info.dtype.names:
                interval_info['start_lat'] = ping_info[interval_lower_bounds]['lat']
                interval_info['end_lat'] = ping_info[interval_upper_bounds]['lat']
                
            if 'lon' in ping_info.dtype.names:
                interval_info['start_lon'] = ping_info[interval_lower_bounds]['lon']
                interval_info['end_lon'] = ping_info[interval_upper_bounds]['lon']
            
                            
            interval_df = \
                pd.DataFrame(interval_info, index=intervals)

        except AttributeError:
            interval_df = pd.DataFrame(dict(first_ping=interval_lower_bounds,
                                            last_ping=interval_upper_bounds),
                                       index=intervals)

        interval_df.index.name = 'interval'
        
        #Construct layers
        adjusted_ranges_per_ping = pd.np.repeat(pd.np.reshape(ranges, (-1, 1)), data.shape[1], axis=1).astype('float32')\
                                     - pd.np.repeat(pd.np.reshape(reference, (1, -1)), data.shape[0], axis=0).astype('float32')

        if self.layer_spacing is None:
            sample_layers_per_ping = (adjusted_ranges_per_ping < 0).astype('int32')

        else:
            sample_layers_per_ping = pd.np.floor(adjusted_ranges_per_ping / self.layer_spacing).astype('int32') + 1

        layers = pd.np.unique(sample_layers_per_ping)
#        return layers, sample_layers_per_ping

        layer_bounds_per_ping = \
            pd.np.empty((data.shape[1], 2*len(layers)), dtype='float32')

        layer_lower_bound = pd.np.zeros(data.shape[1], dtype='float32') + data.axes[0]['sample'][0]
        
        layer_columns = []
        for indx, layer_num in enumerate(layers):
#            nan_indx[~nan_indx] |= data.mask[layer_upper_bound[~nan_indx], ~nan_indx]
            #Exclusive
            #where() replaces all
            
            lower_bound_indx = 2*indx
            upper_bound_indx = lower_bound_indx + 1
            
            layer_upper_bound = (sample_layers_per_ping <= layer_num).sum(axis=0) + data.axes[0]['sample'][0]
            num_layer_samples_per_ping = layer_upper_bound - layer_lower_bound
            nan_indx = num_layer_samples_per_ping == 0
            
            layer_bounds_per_ping[nan_indx, lower_bound_indx] = pd.np.nan
#            layer_bounds_per_ping[~nan_indx, lower_bound_indx] = adjusted_ranges_per_ping[layer_lower_bound[~nan_indx], ~nan_indx] + reference[~nan_indx]
            layer_bounds_per_ping[~nan_indx, lower_bound_indx] = layer_lower_bound[~nan_indx]
            
            nan_indx |= layer_upper_bound >= data.shape[0]
            layer_bounds_per_ping[nan_indx, upper_bound_indx] = pd.np.nan
#            layer_bounds_per_ping[~nan_indx, upper_bound_indx] = adjusted_ranges_per_ping[layer_upper_bound[~nan_indx], ~nan_indx] + reference[~nan_indx]
            layer_bounds_per_ping[~nan_indx, upper_bound_indx] = layer_upper_bound[~nan_indx]
            
            layer_lower_bound = layer_upper_bound
            
            layer_columns.extend([(layer_num, 'lower'), (layer_num, 'upper')])
        
        layer_columns = pd.MultiIndex.from_tuples(layer_columns, names=['layer', 'bound'])
        layer_df = pd.DataFrame(layer_bounds_per_ping, columns=layer_columns)
        layer_df.index.name = 'ping'
        
        
        try:
            ping_axes = pd.to_datetime(data.axes[1]['reduced_timestamp'])
        except AttributeError:
            ping_axes = pd.np.arange(data.shape[1])
            
        sample_axis = pd.DataFrame(ranges, index=data.axes[0]['sample'], columns=['range'])
        sample_axis.index.name = 'sample'
        
        
#        layer_df = layer_df.join(pd.DataFrame(reference, columns=['reference']))
#
#        try:
#            layer_df = layer_df.join(pd.DataFrame(pd.to_datetime(data.axes[1]['reduced_timestamp']), columns=['reduced_timestamp']))
#        except AttributeError:
#            pass

        return Grid(interval_df, layer_df, reference, ping_axes, 
                    sample_axis, interval_unit, layer_unit,
                    self.interval_spacing, self.layer_spacing)


class Grid(object):
    '''
    Grid object used for integration analyses.  Not intended to be
    directly instanced, but instead to be created by calling 
    :func:`GridParameters.grid`
    '''
    def __init__(self, interval_df, layer_df, reference,
                 ping_axes, sample_axis, 
                 interval_unit, layer_unit,
                 interval_spacing, layer_spacing):
        '''
        :param interval_df: Interval dataframe
        :type interval_df: :class:`pandas.DataFrame`

        :param layer_df: Layer dataframe
        :type layer_df: :class:`pandas.DataFrame`

        :param interval_unit:  Interval units
        :type interval_unit: str

        :param layer_unit: Layer units
        :type layer_unit: str

        :param interval_spacing: Spacing between intervals
        :type interval_spacing: float

        :param layer_spacing: Spacing between layers
        :type layer_spacing: float
        '''
        self.intervals_df = interval_df
        self.interval_unit = interval_unit
        self.interval_spacing = interval_spacing

        self.layers_df = layer_df
        self.layer_unit = layer_unit
        self.layer_spacing = layer_spacing
        
        self.reference = reference
        self.ping_axes = ping_axes
        self.sample_axis = sample_axis

    def __getslice__(self, i, j):
        return self.__getitem__(slice(i, j))

    def __getitem__(self, indx):
        
        #slice by layers, i.e. grid[0] or grid[0:3]
        if np.isscalar(indx) or isinstance(indx, (slice, np.ndarray)):
            layer_slice = indx
            interval_slice = None
        
        #Two-dimensional slice, i.e. grid[0, 1:3], grid[0, 0]
        else:
            layer_slice = indx[0]
            interval_slice = indx[1]
            
        
        #Layer slicing first
        layer_id_array = self.layers_df.columns.get_level_values('layer')
        unique_layer_ids = layer_id_array.unique()
        unique_layer_ids.sort()
        
        
        if np.isscalar(layer_slice):
            
            column_ids = np.arange(int(layer_slice), int(layer_slice)+1, 1)
          
        elif isinstance(layer_slice, slice):
            start, stop, step = layer_slice.start, layer_slice.stop, layer_slice.step

            if start is None:
                start = unique_layer_ids[0]

            if stop is None:
                stop = unique_layer_ids.max() + 1                   
            
            elif stop < 0 and stop < unique_layer_ids.min():
                stop = unique_layer_ids.min() - 1
            
            elif stop > 0 and stop > unique_layer_ids.max():
                stop = unique_layer_ids.max() + 1
                
            if step is None:
                step = 1
            
            elif step < 0:
                step = -step
                start, stop = stop, start
                
            column_ids = np.arange(start, stop, step)
            
        else:
            raise TypeError('Unexpected slice type:  %s, this is a bug and needs to be fixed!' %(type(layer_slice)))

        columns = self.layers_df.columns[[x in column_ids for x in layer_id_array]]
        
        if len(columns) == 0:
            raise IndexError('No layers in slice selection')
 
        new_layers_df = self.layers_df[columns]
        
        #Interval slicing
        unique_interval_ids = self.get_interval_ids()
        unique_interval_ids.sort()
        
        if np.isscalar(interval_slice):
            interval_ids = np.arange(int(interval_slice), int(interval_slice)+1, 1)
            
        elif isinstance(interval_slice, slice):
            start, stop, step = interval_slice.start, interval_slice.stop, interval_slice.step

            if start is None:
                start = unique_interval_ids[0]

            if stop is None:
                stop = unique_interval_ids.max() + 1            

            elif stop > 0 and stop > unique_interval_ids.max():
                stop = unique_interval_ids.max() + 1
            
            elif stop < 0:
                raise IndexError('Grid should not have negative interval IDs')
                
            if step is None:
                step = 1
            
            elif step < 0:
                step = -step
                start, stop = stop, start
                
            interval_ids = np.arange(start, stop, step)
        
        elif interval_slice is None:
            pass
    
        else:
            raise TypeError('Unexpected slice type:  %s, this is a bug and needs to be fixed!' %(type(layer_slice)))        
        
        if interval_slice is None:
            new_intervals_df = self.intervals_df.copy()
        
        else:
            new_intervals_df = self.intervals_df.ix[[x in interval_ids for x in self.intervals_df.index]]
            
        if len(new_intervals_df) == 0:
            raise IndexError(u'No intervals in slice selection')

        new_start_ping = new_intervals_df.start_ping.min()
        new_end_ping = new_intervals_df.end_ping.max()
        
        ping_selector = np.zeros((len(self.ping_axes),), dtype=u'bool')
        
        new_interval_start_ping = 0
        for row in new_intervals_df.index.values:
            start_ping = new_intervals_df.start_ping[row]
            end_ping = new_intervals_df.end_ping[row]
            ping_selector[start_ping:end_ping+1] = True
            
            num_pings = end_ping - start_ping + 1
            new_intervals_df.start_ping[row] = new_interval_start_ping
            new_intervals_df.end_ping[row] = new_interval_start_ping + num_pings - 1
            
            new_interval_start_ping += num_pings - 1
    
        new_reference = self.reference[ping_selector]
        new_ping_axes = self.ping_axes[ping_selector]
        
        for column in new_layers_df.columns:
            new_layers_df[column] = new_layers_df[column][ping_selector]
        
        new_layers_df = new_layers_df.dropna(axis=0, how='all').reset_index(drop=True)
        
        return Grid(interval_df=new_intervals_df, 
                    layer_df=new_layers_df, 
                    reference=new_reference, 
                    ping_axes=new_ping_axes, 
                    sample_axis=self.sample_axis[:], 
                    interval_unit=self.interval_unit, 
                    layer_unit=self.layer_unit, 
                    interval_spacing=self.interval_spacing, 
                    layer_spacing=self.layer_spacing)

    def get_layer_ids(self):
        '''
        Returns a list of layer ids
        '''

        return self.layers_df.columns.get_level_values('layer').unique()
    
    def get_interval_ids(self):
        '''
        Returns a list of interval ids
        '''
        return self.intervals_df.index.values

    def get_data_by_interval(self, data, interval):
        '''
        :param data: Data array
        :type data: :class:`echolab.AxesArray.AxesArray`
               
        :param interval:  Interval ID
        :type interval: int
        
        Returns a full interval of data or None if no valid samples
        '''        
        if interval not in self.intervals_df.index:
            raise IndexError('No interval: %s' % (interval))
        
        lb, ub = self.intervals_df.ix[interval][['start_ping', 'end_ping']]
        pings = (data.axes[1]['ping'] >= lb) & (data.axes[1]['ping'] <= ub)
        return data[:, pings].copy().unshare_mask()

    
    def get_data_by_layer(self, data, layer):
        '''
        :param data: Data array
        :type data: :class:`echolab.AxesArray.AxesArray`
               
        :param layer: Layer ID
        :type layer: int
        
        Returns a full layer of data or None if no valid samples
        '''
        if layer not in self.get_layer_ids():
            raise IndexError('No layer: %s' %(layer))
        
        
        data_ping_lb, data_ping_ub = data.axes[1]['ping'][[0, -1]]
        #Row slicing w/ ix is INCLUSIVE, so no need to add +1 to include ping
        #upper bound found above
        
#        layer_bounds = self.layers_df[layer].ix[data_ping_lb:data_ping_ub].dropna(axis=0, how='all')
        data_pings = (self.layers_df.index >= data_ping_lb) & (self.layers_df.index <= data_ping_ub)
        layer_bounds = self.layers_df[layer].ix[data_pings].dropna(axis=0, how='all')
        layer_bounds.reset_index(drop=False, inplace=True)
        
        valid_layer_pings = layer_bounds.shape[0]
        
        #Abort immediately if there's no valid pings
        if valid_layer_pings == 0:
            return None
        
        #Adjust by sample offset in data
        layer_bounds['lower'] = layer_bounds['lower'].fillna(value=self.sample_axis.index[0])
        layer_bounds['upper'] = layer_bounds['upper'].fillna(value=self.sample_axis.index[-1])

        #First array offset for layer bounds
        array_indxs = layer_bounds.ping - data_ping_lb
        
#        #if no match, data does not overlap... which doesn't make sense because
#        #we chose the overlap earlier up at data_ping_*...
#        if array_offset < 0:
#            raise IndexError('Should not error here, this is a bug.')

#        layer_bounds['array_index'] = layer_bounds.ping - array_offset[0]

        #Reset layer_bounds index to 0'based for indexing into data array        
        
        abs_min_layer_bound = int(layer_bounds['lower'].min())
        abs_max_layer_bound = int(layer_bounds['upper'].max())
        
        min_data_bound = data.axes[0]['sample'][0]
        max_data_bound = data.axes[0]['sample'][-1]

        if abs_min_layer_bound <= min_data_bound:
            sample_lb = 0
        elif abs_min_layer_bound in data.axes[0]['sample']:
            sample_lb = pd.np.argwhere(data.axes[0]['sample'] == abs_min_layer_bound)[0]
        else:
            return None
            
        if abs_max_layer_bound >= max_data_bound:
            sample_ub = data.shape[0]
        elif abs_max_layer_bound in data.axes[0]['sample']:
            sample_ub = pd.np.argwhere(data.axes[0]['sample'] == abs_max_layer_bound)[0]
            
        else:
            return None
        
        abs_num_layer_samples = sample_ub - sample_lb
        
        if (layer_bounds['lower'] > min_data_bound).any() or \
                (layer_bounds['upper'] < max_data_bound).any():
            
            sample_matrix = pd.np.repeat(pd.np.reshape(data.axes[0]['sample'][sample_lb:sample_ub], (-1, 1)),
                                         valid_layer_pings, axis=1)
            
            lower_matrix  = pd.np.repeat(pd.np.reshape(layer_bounds['lower'], (1, -1)),
                                         abs_num_layer_samples, axis=0)

            upper_matrix  = pd.np.repeat(pd.np.reshape(layer_bounds['upper'], (1, -1)),
                                         abs_num_layer_samples, axis=0)
            
            layer_bound_mask = (sample_matrix < lower_matrix) | (sample_matrix > upper_matrix)
            
        else:
            layer_bound_mask = False
            
            
        layer_data = data[sample_lb:sample_ub, :].copy().unshare_mask()
        layer_mask = pd.np.ones(layer_data.shape, dtype='bool')
              
        layer_mask[:, array_indxs] = False | layer_bound_mask
        
        layer_data[layer_mask] = pd.np.ma.masked
        
        if layer_data.mask.all():
            return None
        else:
            return layer_data
            
        
    def get_data_by_cell(self, data, interval, layer):
        '''
        :param data: Data array
        :type data: :class:`echolab.AxesArray.AxesArray`
        
        :param interval:  Interval ID
        :type interval: int
        
        :param layer: Layer ID
        :type layer: int
        
        :returns: Data array or None
        
        Get a single (interval, layer) worth of data.
        '''
        
        #get interval bounds
        interval_ping_lb, interval_ping_ub = self.intervals_df.ix[interval][['start_ping', 'end_ping']]
        
        #get absolute bounds on layer
        layer_bounds = self.layers_df[layer].ix[interval_ping_lb:interval_ping_ub].dropna(axis=0, how='all')
#        layer_bounds['upper'] = layer_bounds['upper'].fillna(value=data.shape[0])

        
        #Lower bound

        layer_bounds['lower'] = layer_bounds['lower']
        layer_bounds['upper'] = layer_bounds['upper'].fillna(value=self.sample_axis.index[-1])
        #Reset layer_bounds index to 0'based for indexing into data array       
        valid_layer_pings = layer_bounds.shape[0]
        
        #Abort immediately if there's no valid pings
        if valid_layer_pings == 0:
            return None
        
        abs_min_layer_bound = int(layer_bounds['lower'].min())
        abs_max_layer_bound = int(layer_bounds['upper'].max())
        
        min_data_bound = data.axes[0]['sample'][0]
        max_data_bound = data.axes[0]['sample'][-1]

        if abs_min_layer_bound <= min_data_bound:
            sample_lb = 0
        elif abs_min_layer_bound in data.axes[0]['sample']:
            sample_lb = pd.np.argwhere(data.axes[0]['sample'] == abs_min_layer_bound)[0]
        else:
            return None
            
        if abs_max_layer_bound >= max_data_bound:
            sample_ub = data.shape[0]
        elif abs_max_layer_bound in data.axes[0]['sample']:
            sample_ub = pd.np.argwhere(data.axes[0]['sample'] == abs_max_layer_bound)[0]
            
        else:
            return None
        
                
        return self.get_data_by_layer(data[sample_lb:sample_ub,
                                           interval_ping_lb:interval_ping_ub+1],
                                      layer)
        
        
            
    def plot(self, axes, ref_line_opts=None, layer_line_opts=None,
             interval_line_opts=None, ma_window=20):
        '''
        :param axes: Matplotlib axes instance to draw grid on
        :type axes: :class:`matplotlib.axes.Axes`

        :param ref_line_opts: **kwargs to pass when plotting
            the reference line [#line_opts]_
        :type ref_line_opts: dict

        :param layer_line_opts: **kwargs to pass when plotting layer lines
            [#line_opts]_
        :type layer_line_opts: dict

        :param interval_line_opts: **kwargs to pass when plotting interval
            lines [#line_opts]_
        :type interval_line_opts: dict

        :param ma_window:  Smoothing window layer lines
        :type ma_window: int or :class:`numpy.ndarray`

        :returns: Dictionary of plot objects

        Plots the grid to the provided axes instance.

        Layer lines can be smoothed using the ma_window parameter:
        *  Rectangular windows of used if ma_window is a scalar
        *  Arbitrary window functions can be used by passing an array for
           ma_window

        [#line_opts] See :func:`matplotlib.pyplot.plot` for plot options
        '''
        if ref_line_opts is None:
            ref_line_opts = dict(color='k', lw=4)

        if layer_line_opts is None:
            layer_line_opts = dict(color='gray', linestyle='dashed')

        if interval_line_opts is None:
            interval_line_opts = layer_line_opts

        ref_line = axes.plot(self.reference,
                **ref_line_opts)

        layer_lines = {}

        if (ma_window is not None):
            if np.isscalar(ma_window):
                ma_array = pd.np.ones(ma_window) / ma_window
            else:
                ma_array = ma_window[::-1]
                
            window_skip = len(ma_array) / 2
        else:
            ma_array = None
            

        #filter(lambda x: str(x).startswith('layer'), self.layers_df.columns):
        layers = self.layers_df.columns.get_level_values('layer').unique()
        
        ping_index = self.layers_df.index.values
        layer_values = pd.np.empty_like(ping_index, dtype='float')
        
        for layer in layers:
            
            layer_values[:] = pd.np.nan
                  
            layer_sample_indxs = self.layers_df[layer]['upper'].dropna().astype('int')
            if len(layer_sample_indxs) == 0:
                continue

            layer_ping_indxs = layer_sample_indxs.index.values
#            layer_values[layer_ping_indxs] = self.sample_axis.ix[layer_sample_indxs]
            layer_values[layer_ping_indxs] = self.sample_axis.ix[layer_sample_indxs].values.flatten()
            
            if ma_array is not None:
                #ma_array has already been reversed if an array was
                #passed for ma_window
                smooth_layer_values = pd.np.convolve(layer_values, ma_array,
                                                     'valid')
                
                layer_lines[layer] = axes.plot(ping_index[window_skip:-window_skip+1],
                        smooth_layer_values, **layer_line_opts)
            else:
                
                layer_lines[layer] = axes.plot(ping_index, layer_values,
                    **layer_line_opts)

        y_min, y_max = axes.get_ylim()

        interval_lines = axes.vlines(self.intervals_df.end_ping, y_min,
                y_max, **interval_line_opts)

        axes.get_figure().canvas.draw()

        return dict(ref=ref_line, layers=layer_lines, intervals=interval_lines)



