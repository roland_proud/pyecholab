# coding=utf-8

# Copyright (c) 2012, Zac Berkowitz
#     National Oceanic and Atmospheric Administration (NOAA)
#     Alaskan Fisheries Science Center (AFSC)
#     Resource Assessment and Conservation Engineering (RACE)
#     Midwater Assessment and Conservation Engineering (MACE)

# All rights reserved.

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:

# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.

# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation 
#     and/or other materials provided with the distribution.

# 3.  Neither the names of NOAA, AFSC, RACE, or MACE nor the names of its 
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.


# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

u'''
.. module:: echolab.mask

    :synopsis: Class definitions creating masks.


| Developed by:  Zac Berkowitz <zac.berkowitz@gmail.com> under contract for
| National Oceanic and Atmospheric Administration (NOAA)
| Alaska Fisheries Science Center (AFSC)
| Midwater Assesment and Conservation Engineering Group (MACE)
|
| Maintained by:
|       Zac Berkowitz <zac.berkowitz@gmail.com>
|       Rick Towler   <rick.towler@noaa.gov>
'''

import pandas as pd
import logging

log = logging.getLogger(__name__)

__all__ = ['EchogramMask']

class _Submask(object):
    '''
    Basic submask class.

    Should be subclassed, not to be used directly.

    Classes that build upon _Submask should implement the
    `mask` member function.
    '''
    def __init__(self, mask_type):
        '''
        :param mask_type:  Type of masking (i.e. 'sample', 'ping')
        :type mask_type: str
        '''
        self._enabled = True
        self._mask_type = mask_type

    def mask(self, data):
        '''
        :param data: Data to mask.
        :type data: numpy.ndarray-like

        :returns: boolean mask
        '''
        raise NotImplementedError


    @property
    def is_enabled(self):
        return self._enabled

    @is_enabled.setter
    def is_enabled(self, bool_):
        self._enabled = bool_

    @property
    def mask_type(self):
        return self._mask_type

    def __getslice__(self, i, j):
        return self.__getitem__(slice(i, j))

    def __getitem__(self, indx):
        raise NotImplementedError

class PingFilter(_Submask):
    u'''
    Masks entire pings based on a function applied to a range of sample data.
    '''
    def __init__(self, upper_bound, column_length, bound_unit='sample',
                 threshold=None, deviation=None, mask_direction='<>',
                 method='mean', deviation_type='percent', exclusive=False):
        u'''
        :param upper_bound:  First sample in range (measured from transducer)
        :type upper_bound: num

        :param column_length:  Length of data to sample for analysis
        :type column_length: num

        :param bound_unit:  Type of bound, can be 'sample', 'range', or 'depth'
        :type bound_unit: str

        :param threshold:  value to test filter against
        :type threshold: num

        :param deviation:  allowed deviation from threshold value
        :type deviation: num

        :param deviation_type:  Type of deviation, 'percent' or 'absolute'
        :type deviation_type: str

        :param mask_direction:  Type of mask
        :type mask_direction: str

        :param method:
        :type method: str or callable

        :param exclusive:  Boundaries created by deviation and deviation_type are exclusive.
        :type exclusive: bool



        Mask Directions:
            There are five valid mask directions
            '<'         masks pings whose method returns values less-than threshold - deviation
            '>'         masks pings whose method returns values greater-than threshold - deviation
            '><'        masks pings whose method returns values are outside the bounds defined by
                        deviation and threshold
            '<>'        masks pings whose method returns values are inside the bounds defined by
                        deviation and threshold

        '''
        _Submask.__init__(self, mask_type='ping')

        valid_methods = ['mean', 'median']
        valid_mask_directions = ['<', '>', '><', '<>', '!=']
        valid_deviation_types = ['percent', 'absolute']

        if method.lower() not in valid_methods:
            err_str = \
            '%s is not a valid method, use one of %s' % (method, valid_methods)
            raise ValueError(err_str)

        if mask_direction not in valid_mask_directions:
            err_str = \
            '%s is not a valid mask direction, use one of %s' \
                % (mask_direction, valid_mask_directions)
            raise ValueError(err_str)

        if deviation_type.lower() not in valid_deviation_types:
            err_str = \
            '%s is not a valid mask direction, use one of %s' \
                % (deviation_type, valid_deviation_types)
            raise ValueError(err_str)

        self.method = method.lower()
        self.mask_direction = mask_direction
        self.exclusive = exclusive
        self.upper_bound = upper_bound
        self.column_length = column_length
        self.bound_unit = bound_unit
        self.threshold = threshold
        self.deviation = deviation
        self.deviation_type = deviation_type.lower()

    def __getitem__(self, indx):
        
#        return PingFilter(upper_bound=self.upper_bound, column_length=self.column_length,
#                          bound_unit=self.bound_unit, threshold=self.threshold,
#                          deviation=self.deviation, mask_direction=self.mask_direction,
#                          method=self.method, deviation_type=self.deviation_type,
#                          exclusive=self.exclusive)
        
        if pd.np.isscalar(self.upper_bound):
            upper_bound = self.upper_bound

        else:
            
            if pd.np.isscalar(indx) or isinstance(indx, slice):
                ping_slice = slice(None, None, None)
            
            
            else:
                if len(indx) < 2:
                    ping_slice = slice(None, None, None)
                else:
                    ping_slice = indx[1]
                
            upper_bound = self.upper_bound[ping_slice]
        
        return PingFilter(upper_bound=upper_bound, column_length=self.column_length,
                          bound_unit=self.bound_unit, threshold=self.threshold,
                          deviation=self.deviation, mask_direction=self.mask_direction,
                          method=self.method, deviation_type=self.deviation_type,
                          exclusive=self.exclusive)        
        

    def filter(self, data):

        if pd.np.isscalar(self.upper_bound):
            upper_bound = pd.np.empty((data.shape[1],))
            upper_bound[:] = self.upper_bound

        else:
            if len(self.upper_bound) == data.shape[1]:
                upper_bound = self.upper_bound[:]

            elif len(self.upper_bound) > data.shape[1]:
                upper_bound = self.upper_bound[:data.shape[1]]

            else:
                raise NotImplementedError

        meters_per_sample = data.info['meters_per_sample']
        
        #Convert range bounds to sample bounds
        if self.bound_unit.lower() == 'sample':

#            upper_bound = pd.np.floor(upper_bound * meters_per_sample)
            num_samples = self.column_length
        
        elif self.bound_unit.lower() == 'range':
            upper_bound /= meters_per_sample
            num_samples = int(pd.np.ceil(self.column_length / meters_per_sample))
            
        elif self.bound_unit.lower() == 'depth':
            
            upper_bound = (upper_bound - data.info['transducer_depth'])/meters_per_sample
            num_samples = int(pd.np.ceil(self.column_length / meters_per_sample))
            
        else:
            num_samples = self.column_length

        sample_ub = pd.np.floor(upper_bound).astype('int')
        #Now we have an upper (shallower) bound in samples and a column length
        #in samples.

        if self.method == 'mean':
            column_value_func = pd.np.mean

        elif self.method == 'median':
            column_value_func = pd.np.median

        elif callable(self.method):
            column_value_func = self.method

        else:
            err_str = \
            'Illegal method %s-- should have been caught on init.' % \
                (str(self.method))
            raise RuntimeError(err_str)

        
        column_values = pd.np.ma.MaskedArray(pd.np.empty(data.shape[1],), mask=True)
        
        for bound in pd.np.unique(sample_ub):
            data_ub = (data.axes[0]['sample'] < bound).sum()
            
            #Skip pings where filter range is outside of data range 
            if data_ub >= data.shape[0]:
                continue
            
            data_lb = data_ub + num_samples
            if data_lb > data.shape[0]:
                data_lb = data.shape[0]
                
            #Skip pings w/ no samples to filter
            if data_lb - data_ub < 1:
                continue
                
            ping_selector = sample_ub == bound
            
            column_values[ping_selector] = \
                column_value_func(data[data_ub:data_lb, ping_selector], axis=0)
            

        if self.threshold is None:
            threshold = column_value_func(column_values)
        else:
            threshold = self.threshold


        if self.deviation is None:
            threshold_lb = threshold
            threshold_ub = threshold
            exclusive = True

        elif self.deviation_type == 'percent':
            threshold_ub = threshold * (1 + self.deviation / 100.0)
            threshold_lb = threshold * (1 - self.deviation / 100.0)
            exclusive = self.exclusive
        
        elif self.deviation_type == 'absolute':
            threshold_ub = threshold + self.deviation
            threshold_lb = threshold - self.deviation
            exclusive = self.exclusive
            
        else:
            err_str = \
                'Illegal deviation type %s -- should have caught on init' \
                % (str(self.deviation_type))
            raise RuntimeError(err_str)

        #Swap bounds if inverted
        if threshold_lb > threshold_ub:
            threshold_lb, threshold_ub = threshold_ub, threshold_lb

        #Inside threshold
        if self.mask_direction == '><':
            if exclusive:
                bool_ping_mask = (column_values > threshold_lb) & \
                                 (column_values < threshold_ub)
            else:
                bool_ping_mask = (column_values >= threshold_lb) & \
                                 (column_values <= threshold_ub)

        #outside threshold
        elif self.mask_direction == '<>':
            if exclusive:
                bool_ping_mask = (column_values < threshold_lb) | \
                                 (column_values > threshold_ub)
            else:
                bool_ping_mask = (column_values <= threshold_lb) | \
                                 (column_values >= threshold_ub)

        elif self.mask_direction == '<':
            if exclusive:
                bool_ping_mask = column_values < threshold_lb
            else:
                bool_ping_mask = column_values <= threshold_lb

        elif self.mask_direction == '>':
            if exclusive:
                bool_ping_mask = column_values > threshold_ub
            else:
                bool_ping_mask = column_values >= threshold_ub

        elif self.mask_direction == '!=':
            bool_ping_mask = column_values != threshold

        else:
            err_str = 'Illegal mask direction %s --\
should have been caught on init.' % (str(self.mask_direction))
            raise RuntimeError(err_str)

        return pd.np.arange(data.shape[1])[bool_ping_mask]

    def mask(self, data):
        masked_pings = self.filter(data)

        bool_mask = pd.np.zeros_like(data, dtype='bool')
        bool_mask._mask = bool_mask._mask.copy()
        bool_mask[:, masked_pings] = True
        return bool_mask


class SingleRangeReferenceMask(_Submask):
    '''
    Masks samples above or below a range reference line.
    '''

    def __init__(self, reference, mask_direction='<'):
        '''
        :param reference:  Reference to use in masking
        :param mask_direction:  Masking operation

        `reference` can be a scalar constant or a vector
        of values.

        `mask_direction`:
            '<'  masks samples w/ range less than reference
            '<=' masks samples w/ lange less than or equal to reference
            '>'  masks samples w/ range greater than reference
            '>=  masks samples w/ range greater than or equal to reference
        '''
        _Submask.__init__(self, mask_type='sample')

        valid_directions = ['<', '>', '<=', '>=']
        self.reference = reference
        if mask_direction not in valid_directions:
            raise ValueError('Invalid mask direction. Expected one of %s'\
                              % (valid_directions))

        self.mask_direction = mask_direction

    def __getitem__(self, indx):
        
        if pd.np.isscalar(self.reference):
            return SingleRangeReferenceMask(reference=self.reference, mask_direction=self.mask_direction)
        
        if pd.np.isscalar(indx) or isinstance(indx, slice):
            ping_slice = slice(None, None, None)
            
        else:
            if len(indx) < 2:
                ping_slice = slice(None, None, None)
            else:
                ping_slice = indx[1]
        
        new_reference = self.reference[ping_slice]
        
        return SingleRangeReferenceMask(reference=new_reference, 
                    mask_direction=self.mask_direction)
        
    def mask(self, data):
        '''
        :param data:  Data to be masked
        :type data: echolab.AxesArray
        '''
        if pd.np.isscalar(self.reference):
            reference = pd.np.ones((data.shape[1],)) * self.reference
        else:
            if len(self.reference) == data.shape[1]:
                reference = self.reference

            elif len(self.reference) > data.shape[1]:
                reference = self.reference[:data.shape[1]]

            elif len(self.reference) < data.shape[1]:
                raise NotImplementedError

        bool_mask = pd.np.zeros_like(data, dtype='bool')
        bool_mask._mask = data._mask.copy()
#        bool_mask.harden_mask()
        bool_mask.axes[1]['reference'] = reference
        bool_mask.info.update(dict(mask_type=self.mask_type))

        if self.mask_direction == '<':
            for ping in range(data.shape[1]):
                bool_mask[:, ping] = data.axes[0]['range'] < reference[ping]

        elif self.mask_direction == '<=':
            for ping in range(data.shape[1]):
                bool_mask[:, ping] = data.axes[0]['range'] <= reference[ping]

        elif self.mask_direction == '>':
            for ping in range(data.shape[1]):
                bool_mask[:, ping] = data.axes[0]['range'] > reference[ping]

        elif self.mask_direction == '>=':
            for ping in range(data.shape[1]):
                bool_mask[:, ping] = data.axes[0]['range'] >= reference[ping]

        return bool_mask


class DoubleRangeReferenceMask(_Submask):
    '''
    Masks samples between or outside two range reference lines.
    '''
    def __init__(self, upper, lower, mask_direction='><', exclusive=False):
        '''
        :param upper:  upper (shallower) reference to use in masking
        :param lower:  lower (deeper) reference to use in masking
        :param exclusive:  Exclude reference lines from masked region
        :param mask_direction:  Masking operation

        `upper` and `lower` can be scalar constants or a vectors
        of values.  They should not intersect.

        `mask_direction`:
            '><' masks samples between `upper` and `lower`
            '<>' masks samples outside `upper` and `lower`
        '''
        _Submask.__init__(self, mask_type='sample')

        valid_directions = ['<>', '><']

        if mask_direction not in valid_directions:
            raise ValueError('Invalid mask direction. Expected one of %s'\
                              % (valid_directions))

        if mask_direction == '><':
            if exclusive:
                upper_direction = '>'
                lower_direction = '<'
            else:
                upper_direction = '>='
                lower_direction = '<='

        else:
            if exclusive:
                upper_direction = '<'
                lower_direction = '>'
            else:
                upper_direction = '<='
                lower_direction = '>='

        self.mask_direction = mask_direction
        self.upper_mask = SingleRangeReferenceMask(upper,
                            mask_direction=upper_direction)
        self.lower_mask = SingleRangeReferenceMask(lower,
                            mask_direction=lower_direction)

    def __getitem__(self, indx):
        
        #sloppy, but easy to code.  We create masks twice here
        new_upper_mask = self.upper_mask[indx]
        new_lower_mask = self.lower_mask[indx]
        
        return DoubleRangeReferenceMask(upper=new_upper_mask.reference, 
                        lower=new_lower_mask.reference, mask_direction=self.mask_direction)
        
    @property 
    def upper_bound(self):
        return self.upper_mask.reference
    
    @upper_bound.setter
    def upper_bound(self, reference):
        self.upper_mask.reference = reference
        
    @property
    def lower_bound(self):
        return self.lower_mask.reference
    
    @lower_bound.setter
    def lower_bound(self, reference):
        self.lower_mask.reference = reference

    def mask(self, data):
        '''
        :param data:  Data to be masked
        :type data: echolab.AxesArray
        '''
        if self.mask_direction == '><':
            bool_mask = self.upper_mask.mask(data) & self.lower_mask.mask(data)
        else:
            bool_mask = self.upper_mask.mask(data) | self.lower_mask.mask(data)

        bool_mask.info.update(dict(mask_type=self.mask_type))
        return bool_mask


class ThresholdReferenceMask(_Submask):
    '''
    Masks samples above or below a threshold value.
    '''

    def __init__(self, threshold, mask_direction='<'):
        '''
        :param threshold0:  Reference to use in masking
        :param mask_direction:  Masking operation

        `reference` can be a scalar constant or a vector
        of values.

        `mask_direction`:
            '<'  masks samples w/ value less than threshold
            '<=' masks samples w/ value less than or equal to threshold
            '>'  masks samples w/ value greater than threshold
            '>=  masks samples w/ value greater than or equal to threshold
        '''
        _Submask.__init__(self, mask_type='sample')

        valid_directions = ['<', '>', '<=', '>=']
        self.threshold = threshold
        if mask_direction not in valid_directions:
            raise ValueError('Invalid mask direction. Expected one of %s'\
                              % (valid_directions))

        self.mask_direction = mask_direction

    def mask(self, data):

        if self.mask_direction == '<':
            bool_mask = data < self.threshold

        elif self.mask_direction == '<=':
            bool_mask = data <= self.threshold

        elif self.mask_direction == '>':
            bool_mask = data > self.threshold

        elif self.mask_direction == '>=':
            bool_mask = data >= self.threshold

        bool_mask.info.update(dict(mask_type='sample'))
        return bool_mask

    def __getitem__(self, indx):
        
        return ThresholdReferenceMask(threshold=self.threshold,
                    mask_direction=self.mask_direction)

class _CompositeMask(_Submask):

    def __init__(self):
        
        _Submask.__init__(self, mask_type='composite')
        self.submask = {}

    def __getitem__(self, indx):
        
        new_mask = _CompositeMask()
        
        for name, mask in self.submask.items():
            new_mask.submask[name] = mask[indx]
        
        return new_mask

    def mask(self, data):
        '''
        Creates a boolean mask array by or-ing together all
        enabled submasks
        '''
        bool_mask = pd.np.zeros_like(data, dtype='bool')
        bool_mask._mask = bool_mask._mask.copy()
        
        for submask in self.submask.values():
            if submask.is_enabled:
                bool_mask |= submask.mask(data)

        return bool_mask
    
    @property
    def submask_names(self):
        return self.submask.keys()

    def enable_submask(self, name):
        '''
        Enables submask
        '''
        self.submask[name].is_enabled = True

    def disable_submask(self, name):
        '''
        Disables submask
        '''
        self.submask[name].is_enabled = False

    def disable_all(self):
        '''
        Disables all submasks
        '''
        
        for mask_name in self.submask.keys():
            self.disable_submask(mask_name)

    def enable_all(self):
        '''
        Enables all submasks
        '''
        
        for mask_name in self.submask.keys():
            self.enable_submask(mask_name)

    def add_submask(self, name, submask, overwrite=True):
        '''
        Adds a submask
        '''
        if (not overwrite) and (name in self.submask):
            raise NameError('Already have a submask named %s' % (name))
        self.submask[name] = submask

    def remove_submask(self, name):
        '''
        Removes a submask
        '''
        if name not in self.submask:
            raise NameError('No submask named %s' % (name))

        del self.submask[name]

    def get_submask(self, name):
        '''
        Returns a submask object
        '''
        if name not in self.submask:
            raise NameError('No submask named %s' % (name))

        return self.submask[name]

    def get_masks_of_type(self, mask_type):
        '''
        Returns a new mask object with all submasks of type specified
        '''
        mask_names = [name for (name, mask) in self.submask.items() \
                        if mask.mask_type == mask_type]

        if len(mask_names) == 0:
            return None

        new_mask = _CompositeMask()
        for mask_name in mask_names:
            new_mask.add_submask(mask_name, self.submask[mask_name])

        return new_mask

class EchogramMask(_CompositeMask):

    def __init__(self):
        _CompositeMask.__init__(self)

    def mask_above_range(self, name, reference, exclusive=False):
        if exclusive:
            mask_direction = '<'
        else:
            mask_direction = '<='

        _mask = SingleRangeReferenceMask(reference,
                                         mask_direction=mask_direction)
        self.add_submask(name, _mask)

    def mask_below_range(self, name, reference, exclusive=False):
        if exclusive:
            mask_direction = '>'
        else:
            mask_direction = '>='

        _mask = SingleRangeReferenceMask(reference,
                                         mask_direction=mask_direction)
        self.add_submask(name, _mask)

    def mask_between_ranges(self, name, upper, lower, exclusive=False):

        _mask = DoubleRangeReferenceMask(upper, lower, mask_direction='><',
                                         exclusive=exclusive)
        self.add_submask(name, _mask)

    def mask_outside_ranges(self, name, upper, lower, exclusive=False):

        _mask = DoubleRangeReferenceMask(upper, lower, mask_direction='<>',
                                         exclusive=exclusive)
        self.add_submask(name, _mask)

    def mask_above_threshold(self, threshold, name, exclusive=False):
        if exclusive:
            mask_direction = '>'
        else:
            mask_direction = '>='

        _mask = ThresholdReferenceMask(threshold,
                    mask_direction=mask_direction)
        self.add_submask(name, _mask)

    def mask_below_threshold(self, threshold, name, exclusive=False):
        if exclusive:
            mask_direction = '<'
        else:
            mask_direction = '<='

        _mask = \
        ThresholdReferenceMask(threshold, mask_direction=mask_direction)
        self.add_submask(name, _mask)

    def filter_ringdown(self, name, upper_bound, num_samples,
                        threshold, deviation, deviation_type='percent'):

        _mask = PingFilter(upper_bound, column_length=num_samples,
                           bound_unit='sample', threshold=threshold,
                           deviation=deviation, mask_direction='<>',
                           method='mean', deviation_type=deviation_type,
                           exclusive=False)

        self.add_submask(name, _mask)

    def filter_bottom_return(self, name, upper_bound, num_samples, threshold):
        
        _mask = PingFilter(upper_bound, column_length=num_samples, bound_unit='sample',
                           threshold=threshold, deviation=None, 
                           mask_direction='<', method='median',
                           deviation_type='absolute', 
                           exclusive=False)
        
        self.add_submask(name, _mask)