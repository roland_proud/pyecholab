from distribute_setup import use_setuptools
use_setuptools()

from setuptools import setup, find_packages

LONG_DESCRIPTION=\
"""
echolab is a python 2.7.x library for analyzing acoustic data produced
by SIMRAD EK60, ES60 and ME70 echosounders.

File IO:
    reads/writes raw-data files (.raw, .bot, .out) for EK60 and ES60 echosounders.

Calibration:
    import/export EchoView .ecs calibration files.

Griding:
    Arbitrary referenced layers with 'surface', 'bottom', and 'transducer face' presets

Analysis:
    Single-beam/Split-beam mean Sv integrations

Masking:
    Sample-based masking by threshold
    Range-based masking (exclude above/below/between/outside range-referenced lines)
    Ping-based masking (exclude whole pings based on some function)

Misc:
    Remove triangle-wave offset present in SIMRAD ES60 data
    Check for and fix incorrect clock settings on original acquisition computer
    Interpolate/smooth GPS, vessel distance, and bottom depth values per-ping
"""
setup(
    name='echolab',
    version='2.0',
    author='Zachary Berkowitz',
    author_email='zac.berkowitz@gmail.com',
    maintainer='Rick Towler',
    maintainer_email='rick.towler@noaa.gov',
    description='SIMRAD acoustic data library',
    long_description=LONG_DESCRIPTION,
    license="BSD",
    packages=find_packages(),
    install_requires=['numpy >= 1.7.0',
                      'pandas == 0.14.1',
                      'h5py >= 2.0.0', 
                      'pytz', 
                      'scipy'],
    include_package_data=False,
    zip_safe=True,
    classifiers=['Development Status :: 4 - Beta',
    	'Intended Audience :: Science/Research',
    	'Operating System :: OS Independent',
        'License :: OSI Approved :: BSD License',
    	'Programming Language :: Python :: 2.7',
        'Natural Language :: English',
        'Topic :: Scientific/Engineering',
    	'Topic :: Software Development :: Libraries',
    	'Topic :: Utilities'],
    data_files=['doc']
)
